//
//  AlarmSchedulingViewController.swift
//  AlarmDemo
//
//  Created by vmio vmio on 3/20/19.
//  Copyright © 2019 AlarmDemo. All rights reserved.
//

import UIKit

class AlarmSchedulingViewController: UIViewController {
 @IBOutlet weak var datePic: UIDatePicker!
    @IBOutlet weak var nameTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func Save(_ sender: Any) {
        var date = datePic.date
        let timeInterval = floor(date .timeIntervalSinceReferenceDate / 60.0) * 60.0
        date = NSDate(timeIntervalSinceReferenceDate: timeInterval) as Date
        let alarmItem = AlarmItem(date: date, title: nameTextField.text!, UUID: UUID().uuidString, enabled: true)
        AlarmList.sharedInstance.addItem(alarmItem) // schedule a local notification to persist this item
        
        let demoNotif = UILocalNotification()
            demoNotif.fireDate = NSDate(timeIntervalSinceNow: 10) as Date
            demoNotif.alertBody = "Cục cứt thối hi"
            demoNotif.alertAction = "View"
            demoNotif.applicationIconBadgeNumber = 1
        
        let infoDict = ["mgs" : "You have a date with your girl friend !!"]
        
            demoNotif.userInfo = infoDict
        UIApplication.shared.scheduleLocalNotification(demoNotif)
        
        
    }
    
    
    
}
