//
//  AlarmSchedulerDelegate.swift
//  AlarmDemo
//
//  Created by vmio vmio on 3/20/19.
//  Copyright © 2019 AlarmDemo. All rights reserved.
//

import Foundation

protocol AlarmSchedulerDelegate {
    func setNotificationWithDate(_ item: AlarmItem)
    func removeNotification(_ item: AlarmItem)
    func scheduleReminder(forItem item: AlarmItem)
    func correctDate(_ date: Date) -> Date
}
